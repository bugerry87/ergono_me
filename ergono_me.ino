//Based on:
// I2C device class (I2Cdev) demonstration Arduino sketch for MPU6050 class using DMP (MotionApps v2.0)
// 6/21/2012 by Jeff Rowberg <jeff@rowberg.net>
// Updates should (hopefully) always be available at https://github.com/jrowberg/i2cdevlib
//
// Changelog:
//      2013-05-08 - added seamless Fastwire support
//                 - added note about gyro calibration
//      2012-06-21 - added note about Arduino 1.0.1 + Leonardo compatibility error
//      2012-06-20 - improved FIFO overflow handling and simplified read process
//      2012-06-19 - completely rearranged DMP initialization code and simplification
//      2012-06-13 - pull gyro and accel data from FIFO packet instead of reading directly
//      2012-06-09 - fix broken FIFO read sequence and change interrupt detection to RISING
//      2012-06-05 - add gravity-compensated initial reference frame acceleration output
//                 - add 3D math helper file to DMP6 example sketch
//                 - add Euler output and Yaw/Pitch/Roll output formats
//      2012-06-04 - remove accel offset clearing for better results (thanks Sungon Lee)
//      2012-06-01 - fixed gyro sensitivity to be 2000 deg/sec instead of 250
//      2012-05-30 - basic DMP initialization working

//==========================================================
// Contribution:
//      Gerald Baulig, Denise Junger, Benjamin Weinert, Steffen Wittig
// Changelog:
//      2016-05-25 - Output for Blender.
//      2016-06-02 - Multi MPU usage.
//      2016-06-04 - Disabled interrupt routine.
//		2016-07-06 - Final state.
//==========================================================

#include <avr/wdt.h>

// I2Cdev and MPU6050 must be installed as libraries, or else the .cpp/.h files
// for both classes must be in the include path of your project
#include "I2Cdev.h"

#include "MPU6050_6Axis_MotionApps20.h"
//#include "MPU6050.h" // not necessary if using MotionApps include file

// Arduino Wire library is required if I2Cdev I2CDEV_ARDUINO_WIRE implementation
// is used in I2Cdev.h
#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
    #include "Wire.h"
#endif

#define LED_PIN 13 // (Arduino is 13, Teensy is 11, Teensy++ is 6)

#define MPU_OFFSET 4
#define MPU_COUNT 2

#define MPU_HIP 4
#define MPU_BACK 5

#define VIB_NECK 8
#define VIB_CHEST 9
#define VIB_STOMACH 10

#define HIP 0
#define BACK 1

#define THRESHOLD_MAX 255

// class default I2C address is 0x68
// specific I2C addresses may be passed as a parameter here
// AD0 low = 0x68 (default for SparkFun breakout and InvenSense evaluation board)
// AD0 high = 0x69

MPU6050 mpu[] = {MPU6050(0x69), MPU6050(0x69)}; // <-- use for AD0 high
char* mpuName[] = {"Hip:\t", "Chest:\t"};

//Constants
const float half_pi = M_PI * 0.5f;
const float soll = 0.95f;

// MPU control/status vars
bool valid = false;  // set true if DMP init was successful
uint8_t mpuSwitch = 0;
uint16_t packetSize;    // expected DMP packet size (default is 42 bytes)
uint16_t fifoCount;     // count of all bytes currently in FIFO
uint8_t fifoBuffer[64]; // FIFO storage buffer
uint8_t threshold;

VectorFloat vector[MPU_COUNT];    // [x, y, z]
//float* ypr[] = {new float[3], new float[3], new float[3]};    // [yaw, pitch, roll]   yaw/pitch/roll container and gravity vector

// ================================================================
// ===                      INITIAL HELPER                      ===
// ================================================================

boolean initMPUs() {
  uint8_t devStatus;      // return status after each device operation (0 = success, !0 = error)
  
  for(uint8_t i = 0; i < MPU_COUNT; i++){
    // initialize device
    Serial.print(F("Initializing I2C devices..."));
    Serial.println(mpuSwitch);
    mpu[mpuSwitch].initialize();

    // verify connection
    Serial.println(F("Testing device connections..."));
    if(mpu[mpuSwitch].testConnection()){
      Serial.println(F("MPU6050 connection successful"));
    } else {
      Serial.println(F("MPU6050 connection failed"));
      return false;
    }

    // load and configure the DMP
    Serial.println(F("Initializing DMP..."));
    devStatus = mpu[mpuSwitch].dmpInitialize();

    // supply your own gyro offsets here, scaled for min sensitivity
    mpu[mpuSwitch].setXGyroOffset(220);
    mpu[mpuSwitch].setYGyroOffset(76);
    mpu[mpuSwitch].setZGyroOffset(-85);
    mpu[mpuSwitch].setZAccelOffset(1788); // 1688 factory default for my test chip

    // make sure it worked (returns 0 if so)
    if (devStatus == 0) {
        // turn on the DMP, now that it's ready
        Serial.println(F("Enabling DMP..."));
        mpu[mpuSwitch].setDMPEnabled(true);
        // get expected DMP packet size for later comparison
        packetSize = mpu[mpuSwitch].dmpGetFIFOPacketSize();
    } else {
        // ERROR!
        // 1 = initial memory load failed
        // 2 = DMP configuration updates failed
        // (if it's going to break, usually the code will be 1)
        Serial.print(F("DMP Initialization failed (code "));
        Serial.print(devStatus);
        Serial.println(F(")"));
        return false;
    }
    nextMPU();
  }
  
  return true;
}

void readMPU() {
  Quaternion q;
  VectorFloat v;
  float ypr[MPU_COUNT];
  mpu[mpuSwitch].dmpGetQuaternion(&q, fifoBuffer);
  mpu[mpuSwitch].dmpGetGravity(&v, &q);
  
  vector[mpuSwitch] = v;
}

//for Blender
void printMPU() {
  Serial.print(mpuName[mpuSwitch]);
  Serial.print(vector[mpuSwitch].z * half_pi);
  Serial.print(",");
  Serial.print(vector[mpuSwitch].y * half_pi);
  Serial.print(",");
  Serial.println(0);
}

void nextMPU() {
  // switch MPU-Board here
  mpuSwitch = (mpuSwitch + 1) % MPU_COUNT;
  for(uint8_t i = 0; i < MPU_COUNT; i++){
    digitalWrite(i + MPU_OFFSET, mpuSwitch == i);
  }
}

void feedback(){
  bool neck = vector[BACK].x < soll && vector[BACK].z > 0;
  bool chest = vector[BACK].x < soll && vector[BACK].z > 0;
  bool stomach = vector[HIP].x < soll;
  
  if(neck || chest || stomach){
    threshold = min(threshold +1, THRESHOLD_MAX);  //if pos bad
  } else {
    threshold = max(threshold -1, 0);              //if pos good
  }
  
  if(threshold >= THRESHOLD_MAX){
    digitalWrite(VIB_NECK, neck);
    digitalWrite(VIB_CHEST, chest);
    digitalWrite(VIB_STOMACH, stomach);
  } else {
    digitalWrite(VIB_NECK, false);
    digitalWrite(VIB_CHEST, false);
    digitalWrite(VIB_STOMACH, false);
  }
}

// ================================================================
// ===                      INITIAL SETUP                       ===
// ================================================================

void setup() {
  // configure LED for output
  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, true);
  
  for(int8_t i = 0; i < MPU_COUNT; i++){
    pinMode(i + MPU_OFFSET, OUTPUT);
    digitalWrite(i + MPU_OFFSET, false);
  }
  digitalWrite(MPU_OFFSET, true);
  
  pinMode(VIB_NECK, OUTPUT);
  pinMode(VIB_CHEST, OUTPUT);
  pinMode(VIB_STOMACH, OUTPUT);
  
  digitalWrite(VIB_NECK, false);
  digitalWrite(VIB_CHEST, false); 
  digitalWrite(VIB_STOMACH, false);  

  //Init watchdog. Hurry, we've only 2s!
  wdt_enable(WDTO_2S);
  
  // join I2C bus (I2Cdev library doesn't do this automatically)
  #if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
      Wire.begin();
      TWBR = 24; // 400kHz I2C clock (200kHz if CPU is 8MHz)
  #elif I2CDEV_IMPLEMENTATION == I2CDEV_BUILTIN_FASTWIRE
      Fastwire::setup(400, true);
  #endif

  Serial.begin(115200);
  while (!Serial); // wait for Leonardo enumeration, others continue immediately
  
  valid = initMPUs();
  digitalWrite(LED_PIN, false);
}

// ================================================================
// ===                    MAIN PROGRAM LOOP                     ===
// ================================================================

void loop() {
  // if programming failed, don't try to do anything
  if (!valid) {
    Serial.println("Invalid State!!!");
    digitalWrite(VIB_NECK, false);
    digitalWrite(VIB_CHEST, false); 
    digitalWrite(VIB_STOMACH, false); 
    delay(500);
    return;
  } 
  
  //Reset watchdog, we are still alive!
  wdt_reset(); 
  delay(1);

  // get current FIFO count
  fifoCount = mpu[mpuSwitch].getFIFOCount();

  // check for overflow (this should never happen unless our code is too inefficient)
  if (fifoCount == 1024) {
      // reset so we can continue cleanly
      mpu[mpuSwitch].resetFIFO();
      Serial.println(F("FIFO overflow!"));

  // otherwise, check for DMP data ready interrupt (this should happen frequently)
  } else {
      // wait for correct available data length, should be a VERY short wait
      while (fifoCount < packetSize) {
        fifoCount = mpu[mpuSwitch].getFIFOCount();
      }

      // read a packet from FIFO
      mpu[mpuSwitch].getFIFOBytes(fifoBuffer, packetSize);
      
      // track FIFO count here in case there is > 1 packet available
      // (this lets us immediately read more without waiting for an interrupt)
      fifoCount -= packetSize;
      
      readMPU();
      printMPU();
      feedback();
      nextMPU();
  }
}
