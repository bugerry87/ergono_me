##	Licensed to the Apache Software Foundation (ASF) under one
##	or more contributor license agreements.  See the NOTICE file
##	distributed with this work for additional information
##	regarding copyright ownership.  The ASF licenses this file
##	to you under the Apache License, Version 2.0 (the
##	"License"); you may not use this file except in compliance
##	with the License.  You may obtain a copy of the License at
##
##	  http://www.apache.org/licenses/LICENSE-2.0
##
##	Unless required by applicable law or agreed to in writing,
##	software distributed under the License is distributed on an
##	"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
##	KIND, either express or implied.  See the License for the
##	specific language governing permissions and limitations
##	under the License.

#@author    Gerald Baulig
#           gerald.baulig@gmail.de
#
# Piping rotation data to opject

import sys
import numpy
import re
import bpy

from time import sleep
from threading import Thread
from math import *

__intern__ = __file__ + "\\..\\"
__dir__ = __intern__ + "..\\"

def main():
    print("Ready!");
    for line in sys.stdin:
        pass
        try:
            if re.match("\w+:\s*-?\d+\.?\d*,-?\d+\.?\d*,-?\d+\.?\d*", line):
                print("Rotate:", line)
                splitted = line.split(':')
                new_rot = numpy.array(splitted[1].split(','), dtype=float)
                target = bpy.data.objects[splitted[0]]
                target.rotation_euler = new_rot
            elif re.match("exit", line):
                break
        except:
            pass
    print("Done!");


t = Thread(target=main);
t.start();